core = 7.x
api = 2

; election
projects[election][type] = "module"
projects[election][download][type] = "git"
projects[election][download][url] = "https://git.uwaterloo.ca/drupal-org/election.git"
projects[election][download][tag] = "7.x-1.5"
projects[election][subdir] = ""

; name
projects[name][type] = "module"
projects[name][download][type] = "git"
projects[name][download][url] = "https://git.uwaterloo.ca/drupal-org/name.git"
projects[name][download][tag] = "7.x-1.12"
projects[name][subdir] = ""

; uw_cfg_election
projects[uw_cfg_election][type] = "module"
projects[uw_cfg_election][download][type] = "git"
projects[uw_cfg_election][download][url] = "https://git.uwaterloo.ca/wcms/uw_cfg_election.git"
projects[uw_cfg_election][download][tag] = "7.x-2.2"
projects[uw_cfg_election][subdir] = ""
